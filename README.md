# 贪吃蛇-双蛇大战

#### 介绍
贪吃蛇魔改大赛作品

#### 项目简介
 贪吃蛇已经不满足于普通的食物了，吃到食物居然可以减肥？？猫有九条命，我吃一颗食物就可以多一条命^-^居然有其他设看上了我的领地，看上了我的食物，要和我争抢，这怎么能行！第二条蛇的到来，出发了警报，领地会慢慢缩小，加油，要活下来哦！


#### 游戏规则

1.  操控蛇进行上下左右移动
2.  吃到不同的食物会加不同的分，还会有不同的效果
3.  与第二条蛇相互碰撞可以抢夺对方的身体
4.  第二条蛇不会死亡，自己身体呗掠夺完就会死亡

#### 项目成员

1.  Thkiller（微信号：eugenenamorac）
2.  rac（微信号：racureugene）
3.  asdad（微信号：wxid_icvoowvmu5er22）

#### 项目地址
https://gitee.com/thkiler/greedy_snake

#### 注意事项
1. 背景音乐记得同时拉取哦^_^
2. 界面图片暂未上传，稍候会添加
3. 初学者不太完善，欢迎大家指导修正

### 游戏截图
![输入图片说明](https://images.gitee.com/uploads/images/2020/0807/105140_872ed0e4_5315408.png "4ZD9RWJ]ZS8AE]28%VIC2U1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0807/105229_8b3fc0f2_5315408.png "MAFREN36[GB]%ZPB5SQ}Y%R.png")
#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
